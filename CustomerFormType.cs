using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Entities
{
    public class CustomerFormType
    {
        public Guid CustomerId { get; set; }
        
        public Guid FormTypeId { get; set; }
        
        public virtual Customer Customer { get; set; } = null!;
        public virtual FormType FormType { get; set; } = null!;
    }
}